export class MenuConfig {
  public defaults: any = {
    header: {
      self: {},
      items: [
        {
          title: 'หน้าหลัก',
          root: true,
          icon: 'fas fa-home',
          page: '/home'
        },
      ]
    },
    aside: {
      self: {},
      items: [
        {
          title: '1. หน้าหลัก',
          root: true,
          icon: 'fas fa-home',
          page: '/home'
        },
        {
          title: '2. สมัครสมาชิก',
          root: true,
          bullet: 'dot',
          icon: 'fas fa-user',
          submenu: [
            {
              title: '2.1 สมัครเมมเบอร์',
              page: '/registerMember'
            },
            {
              title: '2.2 สมัครสตาฟ',
              page: '/registerStaff'
            },
          ]
        },
        {
          title: '3. ถอนเงิน',
          root: true,
          icon: 'far fa-money-bill-alt',
          page: '/withdraw'
        },
        {
          title: '4. รายงานสมาชิก',
          root: true,
          icon: 'fas fa-user',
          page: '/transaction/trxlist'
        },
        {
          title: '5. รายงานการฝาก',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/transaction/trxdeposit'
        },
        {
          title: '6. รายงานการถอน',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/transaction/trxwithdraw'
        },
        {
          title: '7. เพิ่ม/ลด เครดิต',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/credit'
        },
        {
          title: '8. รายงานสรุปยอด',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxsummary'
        },
        {
          title: '9. รายงานที่ถูกซ่อน',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxhidden'
        },
        {
          title: '10. พนักงาน',
          root: true,
          icon: 'fas fa-user',
          page: '/staff'
        },
        {
          title: '11. ธนาคาร',
          root: true,
          icon: 'fas fa-university',
          page: '/bank'
        },
        {
          title: '12. รายงานตามสมาชิก',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxlistmember'
        },
        // {
        //   title: 'จัดการโปรโมชั่น',
        //   root: true,
        //   icon: 'fas fa-user',
        //   page: '/managementpromo'
        // },
        // {
        //   title: 'ลิงค์สมัครสมาชิก',
        //   root: true,
        //   icon: 'fas fa-user',
        //   page: '/linkregis'
        // },
        // {
        //   title: 'ยอดได้เสียรายสมาชิก',
        //   root: true,
        //   icon: 'fas fa-list-alt',
        //   page: '/transaction/summarymember'
        // },
      ],
      item2 : [
        {
          title: '1. หน้าหลัก',
          root: true,
          icon: 'fas fa-home',
          page: '/home'
        },
        {
          title: '2. สมัครสมาชิก',
          root: true,
          icon: 'fas fa-user',
          page: '/registerMember'
        },
        {
          title: '3. ถอนเงิน',
          root: true,
          icon: 'far fa-money-bill-alt',
          page: '/withdraw'
        },
        {
          title: '4. รายงานสมาชิก',
          root: true,
          icon: 'fas fa-user',
          page: '/transaction/trxlist'
        },
        {
          title: '5. รายงานการฝาก',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/transaction/trxdeposit'
        },
        {
          title: '6. รายงานการถอน',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/transaction/trxwithdraw'
        },
        {
          title: '7. เพิ่ม/ลด เครดิต',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/credit'
        },
        {
          title: '8. รายงานสรุปยอด',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxsummary'
        },
        {
          title: '9. รายงานที่ถูกซ่อน',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxhidden'
        },
        // {
        //   title: '10. พนักงาน',
        //   root: true,
        //   icon: 'fas fa-user',
        //   page: '/staff'
        // },
        // {
        //   title: '11. ธนาคาร',
        //   root: true,
        //   icon: 'fas fa-university',
        //   page: '/bank'
        // },
        {
          title: '10. รายงานตามสมาชิก',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxlistmember'
        },
        // {
        //   title: 'จัดการโปรโมชั่น',
        //   root: true,
        //   icon: 'fas fa-user',
        //   page: '/managementpromo'
        // },
        // {
        //   title: 'ลิงค์สมัครสมาชิก',
        //   root: true,
        //   icon: 'fas fa-user',
        //   page: '/linkregis'
        // },
        // {
        //   title: 'ยอดได้เสียรายสมาชิก',
        //   root: true,
        //   icon: 'fas fa-list-alt',
        //   page: '/transaction/summarymember'
        // },
      ]
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}
