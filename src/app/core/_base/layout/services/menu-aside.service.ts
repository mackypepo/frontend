// Angular
import { Injectable } from '@angular/core';
// RxJS
import { BehaviorSubject } from 'rxjs';
// Object path
import * as objectPath from 'object-path';
// Services
import { MenuConfigService } from './menu-config.service';

//env
import { environment} from './../../../../../environments/environment'

@Injectable()
export class MenuAsideService {
  // Public properties
  menuList$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

  /**
   * Service constructor
   *
   * @param menuConfigService: MenuConfigService
   */
  constructor(private menuConfigService: MenuConfigService) {
    this.loadMenu();
  }

  /**
   * Load menu list
   */
  loadMenu() {
    // get menu list
    let profile =  JSON.parse(localStorage.getItem('Profile'));
    let status = environment.staff
    // //console.log(status)
    // //console.log(profile.STAFFCLASSID)
    if(status == 'on' && profile.STAFFCLASSID == 3){
    const menuItems: any[] = objectPath.get(this.menuConfigService.getMenus(), 'aside.item2');
    this.menuList$.next(menuItems);
    }else{
      const menuItems: any[] = objectPath.get(this.menuConfigService.getMenus(), 'aside.items');
      this.menuList$.next(menuItems);
    }
  }
}
