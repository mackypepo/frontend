import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import axios from 'axios'
import { environment } from '../../../../../environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

@Component({
  selector: 'kt-trxdeposit',
  templateUrl: './trxdeposit.component.html',
  styleUrls: ['./trxdeposit.component.scss']
})
export class TrxdepositComponent implements OnInit {
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  displayedColumns: string[] = ['BANK_TITLE','COUNT_TD','TOTAL_BALANCE',];
  displayedColumnssum : string[] = 
  ['BANK_TITLE','MEMBER_USERNAME','OA_NAME','BANK_CREATEDATE','CREATEDATE','UPDATEDATE','BALANCE','ACTION_BY','TRANSACTION_GEAR'];
  
  dataSourceSum;

  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  
  websiteAll;
  agentType = "";
  username="";
  Trxdeposit;
  Bankdeposit;
  TRANSACTION_GEAR_AUTO = 0
  TRANSACTION_GEAR_MANUAL =0

  api = environment.apibackend;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState> ) { }

  ngOnInit(): void {
    
    axios({
			method: 'get',
      url: this.api+'/lineoa/all',
      headers: {
				'Authorization': 'Bearer '+this.token
			  },
			})
			.then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
			// do something about response
      this.websiteAll = response.data
     // this.creatDate.setValue(this.daySet)
        }
			})
			.catch(err => {
      console.error(err)
      
      })
      this.onSearch();
  }

  onSearch(){
    let get_frist_date = this.creatDate.value;
    let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
    let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
    let frist_year = get_frist_date.getFullYear();
    let f_date = frist_year + "-" + frist_month + "-" + frist_date;
  
    let get_last_date = this.endDate.value;
    let last_date = ("0" + get_last_date.getDate()).slice(-2);
    let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
    let last_year = get_last_date.getFullYear();
    let l_date = last_year + "-" + last_month + "-" + last_date;
    axios({
      method: 'post',
      url: this.api+'/deposit/findbydate',
      headers: {
				'Authorization': 'Bearer '+this.token
			  },
      data :{
        "USERNAME":this.username,
        "AGENTTYPE":this.agentType,
        "CREATEDATE":f_date,
        "ENDDATE":l_date
      }
      })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
        //console.log(response.data.listTrx)
      // do something about response
      this.Trxdeposit = new MatTableDataSource(response.data.listTrx)
      this.Bankdeposit = response.data.sumBankOwner
      if(response.data.sumtrx.length > 0){
        this.TRANSACTION_GEAR_AUTO = response.data.sumtrx[0].TRANSACTION_GEAR_AUTO
        this.TRANSACTION_GEAR_MANUAL = response.data.sumtrx[0].TRANSACTION_GEAR_MANUAL
      }
      
      
      //this.transactiontop=response.data.sumtrx
      // //console.log(this.transactiontop)
      // this.creatDate.setValue(this.daySet)
      this.Trxdeposit.paginator = this.paginator
      this.changeDetectorRefs.detectChanges();
      //console.log(response.data)
      }
      })
      .catch(err => {
      console.error(err)
      
      })   
   
  }
  

  

}
