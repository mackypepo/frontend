// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// Fake API Angular-in-memory
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
// Translate Module
import { TranslateModule } from '@ngx-translate/core';
// NGRX
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// UI
import { PartialsModule } from '../../partials/partials.module';
// Core
import { FakeApiService } from '../../../core/_base/layout';
// Auth
import { ModuleGuard } from '../../../core/auth';
// Core => Services
import {
	customersReducer,
	CustomerEffects,
	CustomersService,
	productsReducer,
	ProductEffects,
	ProductsService,
	productRemarksReducer,
	ProductRemarkEffects,
	ProductRemarksService,
	productSpecificationsReducer,
	ProductSpecificationEffects,
	ProductSpecificationsService
} from '../../../core/e-commerce';
// Core => Utils
import { HttpUtilsService,
	TypesUtilsService,
	InterceptService,
	LayoutUtilsService
} from '../../../core/_base/crud';
// Shared
import {
	ActionNotificationComponent,
	DeleteEntityDialogComponent,
	FetchEntityDialogComponent,
	UpdateStatusDialogComponent
} from '../../partials/content/crud';
// Components
import { TransactionComponent } from './transaction.component';
// transaction
import { TrxeditComponent } from './trxedit/trxedit.component';
import { TrxlistComponent,DialogtrxlistDeposit,DialogtrxlistWithdraw,DialogtrxlistEdit } from './trxlist/trxlist.component';
import { TrxdepositComponent } from './trxdeposit/trxdeposit.component';
import { TrxwithdrawComponent, DialogContentExampleDialogTrx } from './trxwithdraw/trxwithdraw.component';
import { StaffComponent, DialogStaff } from '../staff/staff.component';
import { BankComponent, DialogBank, DialogAddBank } from '../bank/bank.component';
import { SummarymemberComponent, } from './summarymember/summarymember.component';
import { HomeComponent,HomeDailogTopup, HomeDailogHidden,HomeDailogTopupMessage } from '../home/home.component';
// Products
// import { ProductsListComponent } from '../apps/e-commerce/products/products-list/products-list.component';
// import { ProductEditComponent } from '../apps/e-commerce/products/product-edit/product-edit.component';
// import { RemarksListComponent } from '../apps/e-commerce/products/_subs/remarks/remarks-list/remarks-list.component';
// import { SpecificationsListComponent } from '../apps/e-commerce/products/_subs/specifications/specifications-list/specifications-list.component';
// import { SpecificationEditDialogComponent } from '../apps/e-commerce/products/_subs/specifications/specification-edit/specification-edit-dialog.component';
// Orders
// import { OrdersListComponent } from '../apps/e-commerce/orders/orders-list/orders-list.component';
// import { OrderEditComponent } from '../apps/e-commerce/orders/order-edit/order-edit.component';
// Material
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { environment } from '../../../../environments/environment';
import { NgbProgressbarModule, NgbProgressbarConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxPermissionsModule } from 'ngx-permissions';




// tslint:disable-next-line:class-name
const routes: Routes = [
	{
		path: '',
		component: TransactionComponent,
		// canActivate: [ModuleGuard],
		// data: { moduleName: 'ecommerce' },
		children: [
      {
				path: '',
				redirectTo: 'trxlist',
				pathMatch: 'full'
			},
			{
				path: 'trxlist',
				component: TrxlistComponent
			},
			{
				path: 'trxdeposit',
				component: TrxdepositComponent
			},
			{
				path: 'trxwithdraw',
				component: TrxwithdrawComponent
			},
			{
				path: 'summarymember',
				component: SummarymemberComponent
			},
		]
	}
];

@NgModule({
	imports: [
		MatDialogModule,
		CommonModule,
		HttpClientModule,
		PartialsModule,
		NgxPermissionsModule.forChild(),
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		TranslateModule.forChild(),
		MatButtonModule,
		MatMenuModule,
		MatSelectModule,
        MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
		MatTooltipModule,
		NgbProgressbarModule,
		environment.isMockEnabled ? HttpClientInMemoryWebApiModule.forFeature(FakeApiService, {
			passThruUnknownUrl: true,
        	dataEncapsulation: false
		}) : [],
		StoreModule.forFeature('products', productsReducer),
		EffectsModule.forFeature([ProductEffects]),
		StoreModule.forFeature('customers', customersReducer),
		EffectsModule.forFeature([CustomerEffects]),
		StoreModule.forFeature('productRemarks', productRemarksReducer),
		EffectsModule.forFeature([ProductRemarkEffects]),
		StoreModule.forFeature('productSpecifications', productSpecificationsReducer),
		EffectsModule.forFeature([ProductSpecificationEffects]),
	],
	providers: [
		ModuleGuard,
		InterceptService,
      	{
        	provide: HTTP_INTERCEPTORS,
       	 	useClass: InterceptService,
        	multi: true
      	},
		{
			provide: MAT_DIALOG_DEFAULT_OPTIONS,
			useValue: {
				hasBackdrop: true,
				panelClass: 'mat-dialog-container-wrapper',
				height: 'auto',
				width: '900px'
			}
		},
		TypesUtilsService,
		LayoutUtilsService,
		HttpUtilsService,
		CustomersService,
		ProductRemarksService,
		ProductSpecificationsService,
		ProductsService,
		TypesUtilsService,
		LayoutUtilsService
	],
	entryComponents: [
		HomeDailogTopup,
		HomeDailogHidden,
		HomeDailogTopupMessage,
		DialogContentExampleDialogTrx,
		DialogStaff,
		DialogBank,
		DialogAddBank,
	// Dialogtrxlist
		DialogtrxlistDeposit,
		DialogtrxlistWithdraw,
		DialogtrxlistEdit,
	// 
		ActionNotificationComponent,
		TrxeditComponent,
		DeleteEntityDialogComponent,
		FetchEntityDialogComponent,
		UpdateStatusDialogComponent,
		//SpecificationEditDialogComponent
	],

	declarations: [
		HomeDailogTopup,
		HomeDailogHidden,
		HomeDailogTopupMessage,
		DialogContentExampleDialogTrx,
		DialogStaff,
		DialogBank,
		DialogAddBank,
	// Dialogtrxlist
		DialogtrxlistDeposit,
		DialogtrxlistWithdraw,
		DialogtrxlistEdit,
		// 
		TransactionComponent,
	// transactionmember
		TrxlistComponent,
		TrxeditComponent,
	// transacdeposit
		TrxdepositComponent,
	// transacwithdraw
		TrxwithdrawComponent,
	// staff
		StaffComponent,
	// transaclistmember
	// summarymember
		SummarymemberComponent,
	// Bank
		BankComponent,
	// Home
		HomeComponent
		// Orders
		// OrdersListComponent,
		// OrderEditComponent,
	// Products
		// ProductsListComponent,
		// ProductEditComponent,
		// RemarksListComponent,
		// SpecificationsListComponent,
    // SpecificationEditDialogComponent,
    // TransactionComponent
	]
})
export class TransactionModule { }



