import { Component, OnInit ,ChangeDetectorRef, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { DataSource } from '@angular/cdk/table';
import { FormControl } from '@angular/forms';
import axios from 'axios'
import { environment } from '../../../../../environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'kt-trxwithdraw',
  templateUrl: './trxwithdraw.component.html',
  styleUrls: ['./trxwithdraw.component.scss']
})
export class TrxwithdrawComponent implements OnInit {
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  displayedColumnsFirstTable: string[] =['TRANSACTION_WITHDAW_ID','BANK_NAME','BANK_ACCOUNT_NUMBER','MEMBER_USERNAME','OA_NAME','BALANCE','CREATEDATE','Withdraw','Reject','Return','Hand'];

  displayedColumnsSuccess: string[] =['TRANSACTION_WITHDAW_ID','BANK_ACCOUNT_NUMBER','MEMBER_USERNAME','OA_NAME','BALANCE','CREATEDATE','TRANSACTION_STATUS','ACTION_BY'];
  TrxWithdrawDataTableSecond ;
  displayedColumns: string[] = ['BANK_TITLE','BANK_ACCOUNT_NUMBER','COUNT_TW','TOTAL_BALANCE',];
  transactions;
  actionButton;

  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  Trxwithdrawpending;
  Trxwithdraw;
  bankowner;
  username="";
  websiteAll;
  agentType = "";
  api = environment.apibackend;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog,
    private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState>
     ) {}

  ngOnInit(): void {
    // throw new Error("Method not implemented.");
    axios({
			method: 'get',
      url: this.api+'/lineoa/all',
      headers: {
				'Authorization': 'Bearer '+this.token
			  },
			})
			.then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
          this.websiteAll = response.data
        }
			})
			.catch(err => {
      console.error(err)
      this.changeDetectorRefs.detectChanges();  
      // this.store.dispatch(new Logout());
      })
    //console.log(this.TrxWithdrawDataTableSecond)
    this.onSearch();
    // this.onSerachPending();

  }
 

    onSearch(){
      let get_frist_date = this.creatDate.value;
      let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
      let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
      let frist_year = get_frist_date.getFullYear();
      let f_date = frist_year + "-" + frist_month + "-" + frist_date;
    
      let get_last_date = this.endDate.value;
      let last_date = ("0" + get_last_date.getDate()).slice(-2);
      let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
      let last_year = get_last_date.getFullYear();
      let l_date = last_year + "-" + last_month + "-" + last_date;
      axios({
        method: 'post',
        url: this.api+'/withdraw/findbydate',
        headers: {
          'Authorization': 'Bearer '+this.token
          },
        data :{
          "USERNAME":this.username,
          "AGENTTYPE":this.agentType,
          "CREATEDATE":f_date,
          "ENDDATE":l_date
        }
        })
        .then(response => {
          if(response.data.status == 401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
        // do something about response
        this.Trxwithdraw = new MatTableDataSource(response.data.trxList)
        this.bankowner = response.data.bankCount
        this.Trxwithdraw.paginator = this.paginator
        this.changeDetectorRefs.detectChanges();
        //console.log(this.Trxwithdraw)
          }
        })
        .catch(err => {
        console.error(err) 
        })   
    }

    // onSerachPending(){
    //   axios({
    //     method: 'get',
    //     url: this.api+'/withdraw/findpending',
    //     headers: {
    //       'Authorization': 'Bearer '+this.token
    //       },
    //     })
    //     .then(response => {
    //       if(response.data.status == 401){
    //         alert('มีการล็อคอินใหม่')
    //         this.store.dispatch(new Logout());
    //       }else{
    //     //console.log(response.data)
    //     this.Trxwithdrawpending = new MatTableDataSource(response.data.trxList)
    //     this.Trxwithdrawpending.paginator = this.paginator
    //     this.changeDetectorRefs.detectChanges();
    //     //console.log(this.Trxwithdraw)
    //       }
    //     })
    //     .catch(err => {
    //     console.error(err) 
    //     })   
    // }

  aleartNote(i) {
    //console.log(i)
    alert(i);
    
  }


  //  onReturn(v){
 
  //   for(let index = 0; index < this.Trxwithdrawpending.data.length ; index++){
  //     if(this.Trxwithdrawpending.data[index].TRANSACTION_WITHDAW_ID == v){
  //       //console.log(this.Trxwithdrawpending.data[index])
  //       this.Trxwithdrawpending.data[index].ACTION_STATUS = 1
  //       axios({
      
  //         method: 'post',
  //         url: this.api+'/withdraw/edit',
  //         headers: {
  //           'Authorization': 'Bearer '+this.token
  //         },
  //         data:{
  //           "MEMBER_ID":this.Trxwithdrawpending.data[index].ID,
  //           "MEMBER_USERNAME":this.Trxwithdrawpending.data[index].MEMBER_USERNAME,
  //           "BANK_ACCOUNT_NUMBER":this.Trxwithdrawpending.data[index].BANK_ACCOUNT_NUMBER,
  //           "AMOUNT":this.Trxwithdrawpending.data[index].BALANCE,
  //           "BANK_NAME":this.Trxwithdrawpending.data[index].BANKNAME,
  //           "ACTIONBY":this.profile.STAFFNAME,
  //           "TRX_WITHDRAW_ID":this.Trxwithdrawpending.data[index].TRANSACTION_WITHDAW_ID,
  //           "ACTION":"Return",
  //           "TRANSACTION_STATUS": this.Trxwithdrawpending.data[index].TRANSACTION_STATUS,
  //           "TRANSACTION_GEAR": this.Trxwithdrawpending.data[index].TRANSACTION_GEAR,
  //           "UFABET_STATUS": this.Trxwithdrawpending.data[index].UFABET_STATUS,
  //         }
  //       })
  //         .then(response => {
  //           if(response.data.status ==401){
  //             alert('มีการล็อคอินใหม่')
  //             this.store.dispatch(new Logout());
  //           }else{
            
  //           //console.log("response: ", response.data)
  //           // do something about response
  //           if(response.data == 1){
  //           alert('กำลังทำรายการ รออัปเดทจากทางระบบ')
  //           // this.actionButton = false;
  //           this.onSerachPending();
  //           }else{
  //           alert('พบปัญหากรุณา แจ้งฝ่ายดูแล')
  //           }
  //           }
  //         })
    
  //         .catch(err => {
  //           console.error(err)
           
  //         })
  //     }
  //   }
  // }


  //  onReject(v){
    
  //   for(let index = 0; index < this.Trxwithdrawpending.data.length ; index++){
  //     if(this.Trxwithdrawpending.data[index].TRANSACTION_WITHDAW_ID == v){
  //       //console.log(this.Trxwithdrawpending.data[index])
  //       this.Trxwithdrawpending.data[index].ACTION_STATUS = 1
  //       axios({
      
  //         method: 'post',
  //         url: this.api+'/withdraw/edit',
  //         headers: {
  //           'Authorization': 'Bearer '+this.token
  //         },
  //         data:{
  //           "MEMBER_ID":this.Trxwithdrawpending.data[index].ID,
  //           "MEMBER_USERNAME":this.Trxwithdrawpending.data[index].MEMBER_USERNAME,
  //           "BANK_ACCOUNT_NUMBER":this.Trxwithdrawpending.data[index].BANK_ACCOUNT_NUMBER,
  //           "AMOUNT":this.Trxwithdrawpending.data[index].BALANCE,
  //           "BANK_NAME":this.Trxwithdrawpending.data[index].BANKNAME,
  //           "ACTIONBY":this.profile.STAFFNAME,
  //           "TRX_WITHDRAW_ID":this.Trxwithdrawpending.data[index].TRANSACTION_WITHDAW_ID,
  //           "ACTION":"Reject",
  //           "TRANSACTION_STATUS": this.Trxwithdrawpending.data[index].TRANSACTION_STATUS,
  //           "TRANSACTION_GEAR": this.Trxwithdrawpending.data[index].TRANSACTION_GEAR,
  //           "UFABET_STATUS": this.Trxwithdrawpending.data[index].UFABET_STATUS,
  //         }
  //       })
  //         .then(response => {
  //           if(response.data.status ==401){
  //             alert('มีการล็อคอินใหม่')
  //             this.store.dispatch(new Logout());
  //           }else{
  //           //console.log("response: ", response.data)
  //           // do something about response
  //           if(response.data == 1){
  //             alert('กำลังทำรายการ รออัปเดทจากทางระบบ')
  //             // this.actionButton = false;
  //             this.onSerachPending();
  //             }else{
  //             alert('พบปัญหากรุณา แจ้งฝ่ายดูแล')
  //             }
  //           }
  //         })
    
  //         .catch(err => {
  //           console.error(err)
           
  //         })
  //     }
  //   }
  // }

  
  //  onHand(v){
  //   for(let index = 0; index < this.Trxwithdrawpending.data.length ; index++){
  //     if(this.Trxwithdrawpending.data[index].TRANSACTION_WITHDAW_ID == v){
  //       //console.log(this.Trxwithdrawpending.data[index])
  //       this.Trxwithdrawpending.data[index].ACTION_STATUS = 1
  //       axios({
      
  //         method: 'post',
  //         url: this.api+'/withdraw/edit',
  //         headers: {
  //           'Authorization': 'Bearer '+this.token
  //         },
  //         data:{
  //           "MEMBER_ID":this.Trxwithdrawpending.data[index].ID,
  //           "MEMBER_USERNAME":this.Trxwithdrawpending.data[index].MEMBER_USERNAME,
  //           "BANK_ACCOUNT_NUMBER":this.Trxwithdrawpending.data[index].BANK_ACCOUNT_NUMBER,
  //           "AMOUNT":this.Trxwithdrawpending.data[index].BALANCE,
  //           "BANK_NAME":this.Trxwithdrawpending.data[index].BANKNAME,
  //           "ACTIONBY":this.profile.STAFFNAME,
  //           "TRX_WITHDRAW_ID":this.Trxwithdrawpending.data[index].TRANSACTION_WITHDAW_ID,
  //           "ACTION":"Hand",
  //           "TRANSACTION_STATUS": this.Trxwithdrawpending.data[index].TRANSACTION_STATUS,
  //           "TRANSACTION_GEAR": this.Trxwithdrawpending.data[index].TRANSACTION_GEAR,
  //           "UFABET_STATUS": this.Trxwithdrawpending.data[index].UFABET_STATUS,
  //         }
  //       })
  //         .then(response => {
  //           if(response.data.status ==401){
  //             alert('มีการล็อคอินใหม่')
  //             this.store.dispatch(new Logout());
  //           }else{
  //           //console.log("response: ", response.data)
  //           if(response.data == 1){
  //             alert('กำลังทำรายการ รออัปเดทจากทางระบบ')
  //             // this.actionButton = false;
  //             this.onSearch();
  //             this.onSerachPending();
  //             }else{
  //             alert('พบปัญหากรุณา แจ้งฝ่ายดูแล')
  //             }
  //           }
  //         })
  //         .catch(err => {
  //           console.error(err)
            
  //         })
  //     }
  //   }
     
  // }

  // onWithdraw(v){
  //   //  this.actionButton = true;
  //   for(let index = 0; index < this.Trxwithdrawpending.data.length ; index++){
  //     if(this.Trxwithdrawpending.data[index].TRANSACTION_WITHDAW_ID == v){
  //       //console.log(this.Trxwithdrawpending.data[index])
  //       this.Trxwithdrawpending.data[index].ACTION_STATUS = 1
  //       axios({
      
  //         method: 'post',
  //         url: this.api+'/withdraw/edit',
  //         headers: {
  //           'Authorization': 'Bearer '+this.token
  //         },
  //         data:{
  //           "MEMBER_ID":this.Trxwithdrawpending.data[index].ID,
  //           "MEMBER_USERNAME":this.Trxwithdrawpending.data[index].MEMBER_USERNAME,
  //           "BANK_ACCOUNT_NUMBER":this.Trxwithdrawpending.data[index].BANK_ACCOUNT_NUMBER,
  //           "AMOUNT":this.Trxwithdrawpending.data[index].BALANCE,
  //           "BANK_NAME":this.Trxwithdrawpending.data[index].BANKNAME,
  //           "ACTIONBY":this.profile.STAFFNAME,
  //           "TRX_WITHDRAW_ID":this.Trxwithdrawpending.data[index].TRANSACTION_WITHDAW_ID,
  //           "ACTION":"Withdraw",
  //           "TRANSACTION_STATUS": this.Trxwithdrawpending.data[index].TRANSACTION_STATUS,
  //           "TRANSACTION_GEAR": this.Trxwithdrawpending.data[index].TRANSACTION_GEAR,
  //           "UFABET_STATUS": this.Trxwithdrawpending.data[index].UFABET_STATUS,
  //         }
  //       })
  //         .then(response => {
  //           if(response.data.status ==401){
  //             alert('มีการล็อคอินใหม่')
  //             this.store.dispatch(new Logout());
  //           }else{
  //           //console.log("response: ", response.data)
  //           if(response.data.status == 200){
  //             alert('ทำรายการเสร็จ')
  //             this.onSearch();
  //             this.onSerachPending();
  //             }else{
  //             alert('พบปัญหากรุณา แจ้งฝ่ายดูแล')
  //             }
  //           }
  //         })
  //         .catch(err => {
  //           console.error(err)
            
  //         })
  //     }
  //   }
     
  // }



  bankBalance(i , k){
    let balance = i
    let bankbalance = k
    if(k){
      let p = k.split(/\r?\n/)
      bankbalance = p[0]
    bankbalance = bankbalance.replace(',' ,'')
    bankbalance = bankbalance.replace(',' ,'')
    balance = balance.replace(',','')
    balance = balance.replace(',','')
    let v = parseFloat(bankbalance)
    let t = parseFloat(balance)
    let b = t+v
    return  b.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); ; 
    }
  }
  

  BABankbalance(v){
    if(v){
    let t = v.split(/\r?\n/)
    return t[0]
    }
  }

}

@Component({
  selector:'dialog-content-example-trxwithdraw',
  templateUrl:'dialog-content-example-trxwithdraw.html'
})
  export class DialogContentExampleDialogTrx{}