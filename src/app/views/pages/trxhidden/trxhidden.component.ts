import { Component, OnInit,ElementRef, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms'
import axios from 'axios';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';


@Component({
  selector: 'kt-trxhidden',
  templateUrl: './trxhidden.component.html',
  styleUrls: ['./trxhidden.component.scss']
})
export class TrxhiddenComponent implements OnInit {
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  // datepicker
  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  websiteAll;
  trxhidden;

  // api
  api = environment.apibackend;

@ViewChild('trxHiddenElement',{static:true}) el:ElementRef;

submitted = false;
  constructor(private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState> ) { }

  ngOnInit(): void {
    axios({
			method: 'get',
      url: this.api+'/website/all',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
			})
			.then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
			// do something about response
      this.websiteAll = response.data
     // this.creatDate.setValue(this.daySet)
        }
			})
			.catch(err => {
      console.error(err)
      
      })
      this.onSearch();
     // this.changeDetectorRefs.detectChanges();
  }

  onSearch(){
    let get_frist_date = this.creatDate.value;
    let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
    let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
    let frist_year = get_frist_date.getFullYear();
    let f_date = frist_year + "-" + frist_month + "-" + frist_date;
  
    let get_last_date = this.endDate.value;
    let last_date = ("0" + get_last_date.getDate()).slice(-2);
    let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
    let last_year = get_last_date.getFullYear();
    let l_date = last_year + "-" + last_month + "-" + last_date;
    axios({
      method: 'post',
      url: this.api+'/deposit/trxstatus',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data :{
        "CREATEDATE":f_date,
        "ENDDATE":l_date,
        "TRANSACTION_STATUS":"hiding",
      }
      })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
      // do something about response
      this.trxhidden = response.data.message
      this.changeDetectorRefs.detectChanges();
      //console.log(this.trxhidden)
        }
      })
      .catch(err => {
      console.error(err)
      
      })   
   
  }


  onsubmit(){
    this.submitted=true;
  }
}