import { Component, OnInit,ElementRef, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms'
import axios from 'axios';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

// const trxlistTablefirst: any[] = [
//  {No:'1',promotion: 'ไม่มีโปรโมชั่น', promotionTrans: 22, summary: 40622}
// ];

// const trxlistTablesecond: any[] = [
//   {ID: 1, TRANSACTION_TYPE: 'ลดเครดิต',MEMBER_ID:'ufa1234444',CREDIT:'123123123',CREATEDATE:'31/07/2020 18:29:10',CREATE_BY:'5588morning',
//   REMARK:'fdssfasddasdasdsfa',},
//   {ID: 1, TRANSACTION_TYPE: 'เพิ่มเครดิต',MEMBER_ID:'ufa1234444',CREDIT:'123123123',CREATEDATE:'31/07/2020 18:29:10',CREATE_BY:'5588morning',
//   REMARK:'fdssfasddasdasdsfa',},
//  ];
 
@Component({
  selector: 'kt-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.scss']
})
export class CreditComponent implements OnInit {

@ViewChild('wizard',{static: true}) el: ElementRef;
token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
  displayedColumns: string[] = ['No','promotion','promotionTrans','summary'];
  displayedColumnssum : string[] = 
  ['ID','type','user','creditBefore','Amount','creditAfter','date',
  'by','promotionTable2','notes'];
  displayedColumnsTRX: string[] = ['TRANSACTION_CREDIT_ID','TRANSACTION_TYPE','MEMBER_USERNAME','OA_NAME','CREDIT','CREATEDATE','CREATE_BY','REMARK'];
  dataSource;
  dataSourceSum;
  member;
  submitButton = false ;
  // datepicker
  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  websiteAll;
  agentType = "";
  username="";
  length;
  pageEvent: PageEvent;
  

  // api
  api = environment.apibackend;

  @ViewChild(MatPaginator) paginator: MatPaginator;
 
  model:any={
    uname:'',
    trxType :'',
    credit : '',
    createBy:'',
    remark:'',
  }

  promotionDropdown = new FormControl();
  selectFormControl = new FormControl();
  promotions: any[] = [
    {name: 'รวยจัด'},
    {name: 'รวยจริ้ง'},
  ];

  Chosefor:string;
  
    // seasons = [
    //   'Deposit',
    //   'Withdraw'
    // ];

    seasons: any[] = [
      {name: 'เพิ่มเครดิต' , value:'deposit'},
      {name: 'ลดเครดิต', value:'withdraw'},
    ];

    // day = new Date().getDate()+1
    // setDate = new Date().setDate(this.day)
    // date = new Date(this.setDate)
    // creatDate = new FormControl(new Date());
    // endDate = new FormControl(new Date(this.date));
    // websiteAll;
    // agentType = "";

  constructor(private changeDetectorRefs: ChangeDetectorRef,private store: Store<AppState>) { }

  ngOnInit(): void {
    
    axios({
			method: 'get',
      url: this.api+'/lineoa/all',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
			})
			.then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
          // do something about response
          this.websiteAll = response.data
          // this.creatDate.setValue(this.daySet)
        }
			})
			.catch(err => {
      console.error(err)
      })	

    this.preData();
    this.changeDetectorRefs.detectChanges();
  
  }

  ngAfterViewInit() {
    this.dataSourceSum.paginator = this.paginator;
  }

    onClearData(){
      this.model.uname =''
      this.model.trxType =''
      this.model.credit =''
      this.model.amount =''
      this.model.createBy =''
      this.model.remark =''
      this.member = ''
      this.submitButton = false
    }

    onSearch(){
      //console.log(this.model.uname)
      axios({
        method: 'get',
        url: this.api+'/member/'+this.model.uname,
        headers: {
          'Authorization': 'Bearer '+this.token
        },
      })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
              //console.log("response: ", response.data)
              // do something about response
              this.member = response.data
              //console.log(this.member)
            if(this.member != '' && this.model.uname != ''){
              alert('Username ของลูกค้าสามารถใช่ได้')
              this.submitButton = true;
            }else{
              alert('ไม่พบ Username ของลูกค้า')
            }
        }
        })
        .catch(err => {
          console.error(err)
        })
    }

    async onSubmit(){
      //console.log(this.member)
      //console.log(this.model)
      this.submitButton = false;
     await axios({
        method: 'post',
        url: this.api+'/credit/addcreditapi',
        headers: {
          'Authorization': 'Bearer '+this.token
        },
        data:{
                'MEMBER_ID':this.member.ID,
                'TRANSACTION_TYPE':this.model.trxType,
                'CREDIT':this.model.credit,
                'REMARK':this.model.remark,
                'CREATEDATE':new Date(),
                'CREATEBY': this.profile.STAFFNAME
          }
      })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
            //console.log("response: ", response.data)
            if(response.data.status == "200"){
            this.preData();
            alert('ทำรายการเรียบร้อย')
            this.onClearData();
            }else{
              alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
            }
          }
         
        
        })
        .catch(err => {
          console.error(err)
        //  this.store.dispatch(new Logout());
        })
    }


    async preData(){
      let get_frist_date = this.creatDate.value;
      let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
      let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
      let frist_year = get_frist_date.getFullYear();
      let f_date = frist_year + "-" + frist_month + "-" + frist_date;

      let get_last_date = this.endDate.value;
      let last_date = ("0" + get_last_date.getDate()).slice(-2);
      let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
      let last_year = get_last_date.getFullYear();
      let l_date = last_year + "-" + last_month + "-" + last_date;

       axios({
         method: 'post',
         url: this.api+'/credit/findbycreatedate',
         headers: {
          'Authorization': 'Bearer '+this.token
        },
         data :{
          "USERNAME":this.username,
          "AGENTTYPE":this.agentType,
          "CREATEDATE":f_date,
          "ENDDATE":l_date
        }
       })
         .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
           //console.log("response: ", response.data)
           // do something about response
          this.dataSourceSum = new MatTableDataSource(response.data.message);
          this.dataSourceSum.paginator = this.paginator
          this.changeDetectorRefs.detectChanges();
          }
         })
         .catch(err => {
           console.error(err)
         })
     }

    refresh() {
        this.preData();
        this.changeDetectorRefs.detectChanges();
      };

    }