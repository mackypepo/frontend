import { Component, OnInit ,ElementRef, AfterViewInit, ViewChild} from '@angular/core';
import axios from 'axios'
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { async } from '@angular/core/testing';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';
@Component({
  selector: 'kt-registerStaff',
  templateUrl: './registerStaff.component.html',
  styleUrls: ['./registerStaff.component.scss']
})
export class RegisterStaffComponent implements OnInit , AfterViewInit {

  @ViewChild('wizard', {static: true}) el: ElementRef;
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  regisM: any = {
		uname: '',
		fname: '',
		fpassword:'',
		phone: '',
		class:'',
		depositLimit :''
	};
	submitted = false;
	staffClassAll :any [] = []
	api = environment.apibackend;

	constructor(private store: Store<AppState>) {
	}

	async ngOnInit() {
		await axios({
			method: 'get',
			url: this.api+'/staffclass/all',
			headers: {
				'Authorization': 'Bearer '+this.token
			  }
		  })
			.then(response => {
				if(response.data.status ==401){
					alert('มีการล็อคอินใหม่')
					this.store.dispatch(new Logout());
				  }else{
			  // do something about response
			  this.staffClassAll = response.data
				  }
			})
			.catch(err => {
			  console.error(err)
			  
			})
	}

	ngAfterViewInit(): void {
		// Initialize form wizard
		const wizard = new KTWizard(this.el.nativeElement, {
      startStep: 1
      
		});

		// Validation before going to next page
		wizard.on('beforeNext', (wizardObj) => {
			// https://angular.io/guide/forms
			// https://angular.io/guide/form-validation
			// validate the form and use below function to stop the wizard's step
			if(this.regisM.uname === "" || this.regisM.fname==="" || this.regisM.fpassword ==="" || this.regisM.role===""|| this.regisM.phone==="" ){
			 wizardObj.stop();
			 alert('กรุณากรอกข้อมูลให้ครบ');
			}
		});

		// Change event
		wizard.on('change', (wizard) => {
			setTimeout(() => {
				KTUtil.scrollTop();
			}, 500);
		});
	}

   async onSubmit() {
		this.submitted = true;
		await axios({
			method: 'post',
			url: this.api+'/staff/addstaff',
			headers: {
				'Authorization': 'Bearer '+this.token
			  },
			data :{
				'USERNAME':this.regisM.uname,
                'PASSWORD':this.regisM.fpassword,
                'STAFFNAME':this.regisM.fname,
                'DEPOSITLIMIT':this.regisM.depositLimit,
                'STAFFCLASSID':this.regisM.class,
                'CREATEDATE':new Date(),
                'STATUS_ONLINE':'Y',
                'ACTIVE':'Y'
			}
		  })
			.then(response => {
				if(response.data.status ==401){
					alert('มีการล็อคอินใหม่')
					this.store.dispatch(new Logout());
				  }else{
			  //console.log("response: ", response)
			  // do something about response
			 if(response.data.status == '300' || response.data.status == '301' || response.data.status == '302'){
				alert(response.data.message)
			 }else{
				alert('เพิ่มสตาฟเรียบร้อย')
				window.location.href = "/"
			 }
			}
			})
			.catch(err => {
			  console.error(err)
			 
			})
		  }
	
}
