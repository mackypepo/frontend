import { Component, OnInit, Inject, ChangeDetectorRef,} from '@angular/core'
import { MatDialog } from'@angular/material/dialog';
import axios from 'axios'
import { HttpClient } from '@angular/common/http';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

@Component({
  selector: 'kt-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  DisplayBankdata: string[] = ['BANK_STATUS','Start','Stop','BANK_TITLE','BANK_ACCOUNT_NUMBER','BANK_ACCOUNT_NAME','BANK_TYPE','Edit'];
  dataSource;
  results;
  loading = true;
  api=environment.apibackend;

  constructor(public dialog: MatDialog,
    private http: HttpClient ,
    private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState>) { }
// dailog add bank
  openAddbank(){
    const dialogRef = this.dialog.open(DialogAddBank, {
      height: '45%',
      width: '70%',

    });
  
    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
      this.onTable();
    });
  }

// dailog edit bank
  openBankEditDialog(index) {
    const dialogRef = this.dialog.open(DialogBank, {
      height: '30%',
      width: '50%',
      data:{
        dataKey: this.dataSource[index]
      }
    });
  
    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
      this.onTable();
    });
  }

  ngOnInit(){
    this.onTable();
  }

  async onTable(){
    //console.log('onTable')
     await axios({
        method: 'get',
        url: this.api+'/bankowner/all',
        headers: {
          'Authorization': 'Bearer '+this.token
        },
      })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          //console.log("response: ", response.data)
            // do something about response
          this.dataSource = response.data
          this.changeDetectorRefs.detectChanges();
          }
         
        })
        .catch(err => {
          console.error(err)
        })
  }

  async onStart(index){
    await axios({
      method: 'post',
      url: this.api+'/bankowner/edit',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        'BANK_ACCOUNT_NUMBER':this.dataSource[index].BANK_ACCOUNT_NUMBER,
        'BANK_ACCOUNT_NAME':this.dataSource[index].BANK_ACCOUNT_NAME,
        'BANK_TYPE':this.dataSource[index].BANK_TYPE,
        'CURRENT_BALANCE':this.dataSource[index].CURRENT_BALANCE,
        'BANK_STATUS':'Y',
        'ID':this.dataSource[index].ID,
        'BANK_TITLE':this.dataSource[index].BANK_TITLE
      }
    })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
        //console.log("response: ", response.data)
        this.onTable();
        }
      })
      .catch(err => {
        console.error(err)
       
      })
  }
  async onStop(index){
    await axios({
      method: 'post',
      url: this.api+'/bankowner/edit',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        'BANK_ACCOUNT_NUMBER':this.dataSource[index].BANK_ACCOUNT_NUMBER,
        'BANK_ACCOUNT_NAME':this.dataSource[index].BANK_ACCOUNT_NAME,
        'BANK_TYPE':this.dataSource[index].BANK_TYPE,
        'CURRENT_BALANCE':this.dataSource[index].CURRENT_BALANCE,
        'BANK_STATUS':'N',
        'ID':this.dataSource[index].ID,
        'BANK_TITLE':this.dataSource[index].BANK_TITLE
      }
    })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
          //console.log("response: ", response.data)
          this.onTable();
        }
      
      })
      .catch(err => {
        console.error(err)
      })
  }

}

@Component({
  selector:'dialog-bank',
  templateUrl:'dialog-bank.html'
})
  export class DialogBank implements OnInit{
    token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
    api=environment.apibackend;
    dialogRef: any;
    constructor(
      @Inject(MAT_DIALOG_DATA) public data: any , 
      private matDialog: MatDialog,
      private store: Store<AppState>
   ){

   }

    
    // dialog add bank
    // regisBank: any = {
    //                     BankTitle: '',
    //                     BankType:'',
    //                     BankAccountName: '',
    //                     BankAccountNumber: '',
    //                   };
    // addbankdata;
    // addbanktype=['Deposit','Withdraw' 
    // ];
    
    // dailog edit bank
    bankdata;
    banktype=[
      {name: 'Deposit', value: 'deposit'},
      {name: 'Withdraw', value: 'withdraw'},
    ];

    ngOnInit(){
      this.data.dataKey
      this.bankdata = this.data.dataKey
      //console.log(this.bankdata)
    }


  async onSubmit(){
    //console.log(this.bankdata)
    await axios({
      method: 'post',
      url: this.api+'/bankowner/edit',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        'BANK_ACCOUNT_NUMBER':this.bankdata.BANK_ACCOUNT_NUMBER,
        'BANK_ACCOUNT_NAME':this.bankdata.BANK_ACCOUNT_NAME,
        'BANK_TYPE':this.bankdata.BANK_TYPE,
        'CURRENT_BALANCE':this.bankdata.CURRENT_BALANCE,
        'BANK_STATUS':this.bankdata.BANK_STATUS,
        'ID':this.bankdata.ID,
        'BANK_TITLE':this.bankdata.BANK_TITLE
      }
    })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
          //console.log("response: ", response.data)
          // do something about response
          if(response.data.message == "Success"){
            alert('แก้ไขเรียบร้อย')
            this.matDialog.closeAll();
          }else{
            alert('ไม่สามารถทำรายการได้กรุณารอสักครู่ แล้วทำรายการใหม่')
          }
        }
        
      })
      .catch(err => {
        console.error(err)
      })
   }


}

@Component({
  selector:'dialog-addbank',
  templateUrl:'dialog-addbank.html'
})
  export class DialogAddBank implements OnInit{
    token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
    api=environment.apibackend;
    dialogRef: any;
    constructor(
      @Inject(MAT_DIALOG_DATA) public data: any , 
      private matDialog: MatDialog,
      private changeDetectorRefs: ChangeDetectorRef,
      private store: Store<AppState>
   ){

   }

    bankdata;
    banktype=[
      {name: 'Deposit', value: 'deposit'},
      {name: 'Withdraw', value: 'withdraw'},
    ];

    bankstatus=[
      {name: 'Active', value: 'Y'},
      {name: 'Inactive', value: 'N'},
    ];

    bankgear=[
      {name: 'Auto', value: 'auto'},
      {name: 'Manual', value: 'manual'},
    ];

    body=
      {BANK_ACCOUNT_NUMBER: '',
      BANK_ACCOUNT_NAME: '',
      BANK_TYPE:'',
      CURRENT_BALANCE:'0',
      BANK_STATUS:'',
      BANK_TITLE:'',
      BANK_GEAR:'',
      BANK_USERNAME:'',
      BANK_PASSWORD:''
    };

    bankAll;

    ngOnInit(){
     axios({
        method: 'get',
        url: this.api+'/bank/all',
        headers: {
          'Authorization': 'Bearer '+this.token
        },
      })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          //console.log("response: ", response.data)
            // do something about response
          this.bankAll = response.data
          this.changeDetectorRefs.detectChanges();
          }
         
        })
        .catch(err => {
          console.error(err)
        })
    }


  async onSubmit(){
    //console.log(this.body)
  if(this.body.BANK_ACCOUNT_NUMBER != '' && 
  this.body.BANK_ACCOUNT_NUMBER !='' &&
  this.body.BANK_TYPE !='' && 
  this.body.CURRENT_BALANCE !=''&&
  this.body.BANK_STATUS !=''&&
  this.body.BANK_TITLE !=''&&
  this.body.BANK_GEAR !=''){
    //console.log(this.body.BANK_TYPE)
    if((this.body.BANK_TYPE == 'withdraw' &&
    this.body.BANK_USERNAME !='' &&
    this.body.BANK_PASSWORD !='') || this.body.BANK_TYPE == 'deposit'){
    //console.log(this.bankdata)
    await axios({
      method: 'post',
      url: this.api+'/bankowner/add',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        'BANK_ACCOUNT_NUMBER':this.body.BANK_ACCOUNT_NUMBER,
        'BANK_ACCOUNT_NAME':this.body.BANK_ACCOUNT_NAME,
        'BANK_TYPE':this.body.BANK_TYPE,
        'CURRENT_BALANCE':this.body.CURRENT_BALANCE,
        'BANK_STATUS':this.body.BANK_STATUS,
        'BANK_TITLE':this.body.BANK_TITLE,
        'BANK_GEAR':this.body.BANK_GEAR,
        'BANK_USERNAME':this.body.BANK_USERNAME,
        'BANK_PASSWORD':this.body.BANK_PASSWORD,
      }
    })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
          //console.log("response: ", response.data)
          // do something about response
          if(response.data.message){
            alert('เพิ่มเรียบร้อย')
            this.matDialog.closeAll();
          }else{
            alert('ไม่สามารถทำรายการได้กรุณารอสักครู่ แล้วทำรายการใหม่')
          }
        }
       
      })
      .catch(err => {
        console.error(err)
      })
    }else{
      alert('กรุณากรอกข้อมูล Username และ Password')
    }
    }else{
      alert('กรุณากรอกข้อมูลให้ครบ')
    }
   }
 

}


