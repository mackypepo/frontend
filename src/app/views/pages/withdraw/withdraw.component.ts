import { Component, OnInit, ElementRef, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import axios from 'axios'
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

@Component({
  selector: 'kt-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit, AfterViewInit {

  @ViewChild('wizard', {static: true}) el: ElementRef;
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
    displayDeposit:string[]=['ID','BALANCE','REMARK']
	dataSoure;
	submitButton = false;
	api = environment.apibackend;
	sbb;
	theCheckbox = false;

  withD: any = {
	   	id :'',
		uname: '',
		fname: '',
		lname:'',
		bankaccount: '',
		bank:'',
		amount:'',
	};
	submitted = false;

	constructor(private changeDetectorRefs: ChangeDetectorRef,
		private store: Store<AppState>) {
	}

	ngOnInit() {
	}

	ngAfterViewInit(): void {
		// Initialize form wizard
		const wizard = new KTWizard(this.el.nativeElement, {
      startStep: 1
      
		});

		// Validation before going to next page
		wizard.on('beforeNext', (wizardObj) => {
			// https://angular.io/guide/forms
			// https://angular.io/guide/form-validation
			// validate the form and use below function to stop the wizard's step
			if(this.withD.uname === ""){
			 wizardObj.stop();
			 alert('กรุณากรอกข้อมูลให้ครบ');
			}
		});

		// Change event
		wizard.on('change', (wizard) => {
			setTimeout(() => {
				KTUtil.scrollTop();
			}, 500);
		});
	}

	onSubmit(body) {
		//console.log(body);
		this.sbb = false;
		if(this.theCheckbox== true){
			//console.log(this.theCheckbox)
			axios({
				method: 'post',
				url: this.api+'/withdraw/hand',
				headers: {
					'Authorization': 'Bearer '+this.token
				  },
				data :{
					"ID":this.withD.id,
					"AMOUNT":this.withD.amount,
					"ACTIONBY":this.profile.STAFFNAME
				}
				  })
				.then(response => {
					if(response.data.status ==401){
						alert('มีการล็อคอินใหม่')
						this.store.dispatch(new Logout());
					  }else{
				  // do something about response
					//console.log(response)
					if(response.data.status == '200'){
						alert(response.data.message)
						this.onClearData();
					}else{
						alert('ระบบมีปัญหากรุณารอสักครู่ แล้วทำรายการใหม่')
						this.onClearData();
					}
				}	
				})
				.catch(err => {
					console.error(err)
					
				})
		}else{
		axios({
			method: 'post',
			url: this.api+'/withdraw/trx',
			headers: {
				'Authorization': 'Bearer '+this.token
			  },
			data :{
				"ID":this.withD.id,
				"MEMBER_USERNAME":this.withD.uname,
				"BANK_ACCOUNT_NUMBER":this.withD.bankaccount,
				"AMOUNT":this.withD.amount,
				"BANK_NAME":this.withD.bank,
				"ACTIONBY":this.profile.STAFFNAME
			}
		  	})
			.then(response => {
				
				if(response.data.status ==401){
					alert('มีการล็อคอินใหม่')
					this.store.dispatch(new Logout());
				  }else{
			  // do something about response
				//console.log(response.data)
				if(response.data.status == '200'){
					alert(response.data.message)
					this.onClearData();
				}else{
					alert('ระบบถอนเครดิตทาง UFA มีปัญหากรุณารอสักครู่ แล้วทำรายการใหม่')
					this.onClearData();
				}
			}	
			})
			.catch(err => {
				console.error(err)
				
			})
		}
	}

	onClearData(){
		this.withD.id = ''
		this.withD.uname = ''
		this.withD.fname = ''
		this.withD.lname = ''
		this.withD.bankaccount = ''
		this.withD.bank= ''
		this.withD.amount= ''
		this.dataSoure = []
		this.submitButton = false
		this.sbb = ''
		this.theCheckbox = false;
	}

	 onSerach(){
		axios({
			method: 'get',
			url: this.api+'/member/'+this.withD.uname,
			headers: {
				'Authorization': 'Bearer '+this.token
			  },
		  })
			.then(response => {
				if(response.data.status ==401){
					alert('มีการล็อคอินใหม่')
					this.store.dispatch(new Logout());
				  }else{
			  // do something about response
				//console.log(response.data)
				if(response.data){
					this.withD.id = response.data.ID
					this.withD.fname = response.data.MEMBERFIRSTNAME
					this.withD.lname = response.data.MEMBERLASTNAME
					this.withD.bankaccount = response.data.BANK_ACCOUNT_NUMBER
					this.withD.bank= response.data.BANKNAME
					this.withD.amount= 0
					this.changeDetectorRefs.detectChanges();
					//console.log(this.withD)
					axios({
						method: 'get',
						url: this.api+'/withdraw/all/'+response.data.ID,
						headers: {
							'Authorization': 'Bearer '+this.token
						  },
					})
					 .then(response => { 
						if(response.data.status ==401){
							alert('มีการล็อคอินใหม่')
							this.store.dispatch(new Logout());
						  }else{
						this.dataSoure = response.data
						//console.log(this.dataSoure)
						this.submitButton = true;
						this.changeDetectorRefs.detectChanges();
						  }
					})
					.catch(err => {
						console.error(err)
					
					})

				}else{
					alert('ไม่พบ Username ลูกค้าในระบบ รบกวนตรวจสอบ Username')
					this.onClearData();
				}
			}
			})
			.catch(err => {
			  console.error(err)
			})
	}
}
