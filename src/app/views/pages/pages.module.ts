// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
import { MailModule } from './apps/mail/mail.module';
import { ECommerceModule } from './apps/e-commerce/e-commerce.module';
import { UserManagementModule } from './user-management/user-management.module';
import { MyPageComponent } from './my-page/my-page.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { CreditComponent } from './credit/credit.component';
import { TrxlistmemberComponent } from './trxlistmember/trxlistmember.component'
import { TrxsummaryComponent } from './trxsummary/trxsummary.component';
import { TrxhiddenComponent } from './trxhidden/trxhidden.component';
import { HistorymemberComponent } from './historymember/historymember.component';
import { ManagementpromoComponent } from './managementpromo/managementpromo.component';
import { AdsComponent } from './ads/ads.component';
import { DepositComponent } from './deposit/deposit.component';
import { LinkregisComponent } from './linkregis/linkregis.component';
import { TransactionModule } from './transaction/transaction.module';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRadioModule } from '@angular/material/radio';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule } from '@angular/material/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { InlineSVGModule } from 'ng-inline-svg';
import { RegisterMemberComponent } from './registerMember/registerMember.component';
import { RegisterStaffComponent } from './registerStaff/registerStaff.component';
import { PusherService } from 'src/app/pusher.service';



@NgModule({
  declarations: [MyPageComponent, WithdrawComponent, CreditComponent, TrxsummaryComponent, TrxhiddenComponent, 
    HistorymemberComponent, ManagementpromoComponent, AdsComponent, RegisterStaffComponent, TrxlistmemberComponent,
    DepositComponent, LinkregisComponent, RegisterMemberComponent],
  exports: [],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    PartialsModule,
    MailModule,
    ECommerceModule,
    UserManagementModule,
    TransactionModule,
    MatButtonModule,
		MatMenuModule,
		MatSelectModule,
    MatInputModule,
		MatTableModule,
		MatAutocompleteModule,
		MatRadioModule,
		MatIconModule,
		MatNativeDateModule,
		MatProgressBarModule,
		MatDatepickerModule,
		MatCardModule,
		MatPaginatorModule,
		MatSortModule,
		MatCheckboxModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatTabsModule,
    MatTooltipModule,
    InlineSVGModule,
    
    
  ],
  providers: [PusherService]
})
export class PagesModule {
}
