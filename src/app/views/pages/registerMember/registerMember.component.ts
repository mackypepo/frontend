import { Component, OnInit ,ElementRef, AfterViewInit, ViewChild} from '@angular/core';
import axios from 'axios'
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

@Component({
  selector: 'kt-registerMember',
  templateUrl: './registerMember.component.html',
  styleUrls: ['./registerMember.component.scss']
})
export class RegisterMemberComponent implements OnInit , AfterViewInit {

  @ViewChild('wizard', {static: true}) el: ElementRef;
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  regisM: any = {
  Fname: '',
  Lname: '',
  AgenType:'',
  AccountNumber: '',
  Bank:'',
  Phone:'',
  Recommend:'',
  Notes:'',
  LineOa :'',
  ActionError :'',
  };
    bankAll : any[] = []
  channelAll : any[] = []
  websiteAll : any[] = []
  lineoaAll : any[] = []
  submitted = false;
  api = environment.apibackend;

  constructor( private store: Store<AppState> ) {
  }

  async ngOnInit() {
   // //console.log(this.profile.USERNAME)
   await axios({
      method: 'get',
      url: this.api+'/bank/all',
      headers: {
        'Authorization': 'Bearer '+this.token
      }
      })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
        // do something about response
        this.bankAll = response.data
        }
      })
      .catch(err => {
        console.error(err)
      })

   await axios({
      method: 'get',
      url: this.api+'/channel/all',
      headers: {
        'Authorization': 'Bearer '+this.token
      }
      })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
        // do something about response
      this.channelAll = response.data
        }
      })
      .catch(err => {
      console.error(err)
      })  
   await axios({
      method: 'get',
      url: this.api+'/website/all',
      headers: {
        'Authorization': 'Bearer '+this.token
      }
      })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
      // do something about response
      this.websiteAll = response.data
        }
      })
      .catch(err => {
      console.error(err)
      //this.store.dispatch(new Logout());
      })  
      
      await axios({
        method: 'get',
        url: this.api+'/lineoa/all',
        headers: {
          'Authorization': 'Bearer '+this.token
        }
        })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          // do something about response
        this.lineoaAll = response.data
          }
        })
        .catch(err => {
        console.error(err)
        })  

    }
   
  ngAfterViewInit(): void {
    // Initialize form wizard
    const wizard = new KTWizard(this.el.nativeElement, {
      startStep: 1
  });

    // Validation before going to next page
    wizard.on('beforeNext', (wizardObj) => {
      // validate the form and use below function to stop the wizard's step
      if(this.regisM.Fname === "" || 
         this.regisM.Lname==="" ||
         this.regisM.AgenType ==="" || 
         this.regisM.AccountNumber===""|| 
         this.regisM.Bank==="" || 
         this.regisM.Phone==="" || 
         this.regisM.Recommend==="" ||
         this.regisM.LineOa ==="" ){
       wizardObj.stop();
       alert('กรุณากรอกข้อมูลให้ครบ');
      }
    });

    // Change event
    wizard.on('change', (wizard) => {
      setTimeout(() => {
        KTUtil.scrollTop();
      }, 500);
    });
  }

  async onSubmit() {
    this.submitted = true;
    let username = ''
    let websiteCode
    for(let web of this.websiteAll){
      if(web.ID == this.regisM.AgenType){
        //console.log(web.WEBSITECODE)
        username = web.WEBSITECODE + 1
        websiteCode = web.WEBSITECODE+1 
      }
    }
    //console.log(username)
    let mh = this.regisM.AccountNumber.replace(/ /g,'')
    await axios({
      method: 'post',
      url: this.api+'/member/addmember',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data :{
        'MEMBER_USERNAME':username,
        'MEMBERFIRSTNAME':this.regisM.Fname,
        'MEMBERLASTNAME':this.regisM.Lname,
        'PHONE':this.regisM.Phone,
        'BANK_ID':this.regisM.Bank,
        'BANK_ACCOUNT_NUMBER':mh,
        'BANK_ACCOUNT_NAME':this.regisM.Fname+ " " +this.regisM.Lname,
        'WEBSITE_ID':this.regisM.AgenType,
        'OA_ID':this.regisM.LineOa,
        'CHANELID':this.regisM.Recommend,
        'CREDITE':"0",
        'CREATEBY':this.profile.ID,
        'WEBSITECODE':websiteCode,
        'ACTIONERROR':this.regisM.ActionError,
        'CREATEDATE':new Date()  
      }
      })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
        //console.log("response: ", response)
        // do something about response
       if(response.data.status == '300' || response.data.status == '301' || response.data.status == '302' || response.data.status == '304'){
        alert(response.data.message)
       }else{
         //console.log(response.data)
        alert('เพิ่มแมมเบอร์เรียบร้อย ID : '+response.data.username + ' Password : '+response.data.password )
        window.location.href = "/"
       }
      }
      })
      .catch(err => {
        console.error(err)
      
      })
      }
  

}