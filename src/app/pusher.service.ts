declare const Pusher: any;
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
 
@Injectable({
  providedIn: 'root'
})


 
export class PusherService {
  pusher: any;
  channel: any;
  key=environment.pusher.key;
  cluster=environment.pusher.cluster
  constructor(private https: HttpClient) {
    this.pusher = new Pusher(this.key, {
      cluster: this.cluster
    });
    this.channel = this.pusher.subscribe('my-channel');
  }
}